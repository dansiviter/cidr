/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter;

import static java.math.BigInteger.ONE;
import static java.util.Objects.hash;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;

/**
 * This class represents a Classless Inter-Domain Routing (CIDR). This is
 * generally useful for things such as validating rules through HTTP filters.
 *
 * @author Daniel Siviter
 * @since v1.0 [16 Feb 2020]
 */
public class Cidr implements Comparable<Cidr> {
	private final boolean ipv6;
	private final int suffix;
	private final BigInteger netmask;
	private final BigInteger start;
	private final BigInteger end;
	private final InetAddress startAddr;

	private Cidr(InetAddress addr, int suffix) {
		this.ipv6 = addr instanceof Inet6Address;
		this.suffix = suffix;
		this.netmask = length().subtract(ONE).not();
		this.start = toBigInt(addr).and(this.netmask);
		this.end = this.start.add(length()).subtract(ONE);
		this.startAddr = toAddress(this.start);
	}

	/**
	 * @return the start address represented by this.
	 */
	public InetAddress start() {
		return startAddr;
	}

	/**
	 * @return the end address represented by this.
	 */
	public InetAddress end() {
		return toAddress(this.end);
	}

	/**
	 * @return the netmask.
	 */
	public InetAddress netmask() {
		return toAddress(this.netmask);
	}

	/**
	 * @return the suffix representing a count of the bits in the netmask.
	 */
	public int suffix() {
		return suffix;
	}

	/**
	 * @return the number of potential addresses this represents.
	 */
	public BigInteger length() {
		return ONE.shiftLeft((this.ipv6 ? 128 : 32) - this.suffix);
	}

	/**
	 * Tests whether the given address is within the address space this CIDR
	 * represents.
	 *
	 * @param addr the address to test.
	 * @return {@code true} if this is within this
	 */
	public boolean contains(InetAddress addr) {
		final BigInteger search = toBigInt(addr);
		return search.compareTo(start) >= 0 && search.compareTo(end) <= 0;
	}

	private BigInteger toBigInt(InetAddress addr) {
		final byte[] bytes;
		if (ipv6 && addr instanceof Inet4Address) {
			bytes = toIPv6Bytes((Inet4Address) addr);
		} else if (!ipv6 && addr instanceof Inet6Address) {
			bytes = toIPv4Bytes((Inet6Address) addr);
		} else {
			bytes = addr.getAddress();
		}
		return new BigInteger(bytes);
	}

	private InetAddress toAddress(BigInteger addr) {
		return ipv6 ? toIPv6Address(addr) : toIPv4Address(addr);
	}

	@Override
	public int compareTo(Cidr o) {
		if (this.ipv6 != o.ipv6) {
			final BigInteger net = toBigInt(o.startAddr);
			final int res = net.compareTo(start);
			if (res != 0) {
				return res;
			}
			return Integer.compare(o.suffix, suffix);
		}
		final int res = o.start.compareTo(start);
		if (res != 0) {
			return res;
		}
		return Integer.compare(o.suffix, suffix);
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Cidr) {
			return compareTo((Cidr) obj) == 0;
		}
		return false;
	}

	@Override
	public int hashCode() {
		return hash(this.start, this.suffix);
	}

	@Override
	public String toString() {
		return this.startAddr.getHostAddress() + "/" + suffix;
	}


	// --- Static Methods ---

	/**
	 *
	 * @param addr the base IP address.
	 * @param suffix the netmask suffix.
	 * @return a new instance representing arguments.
	 * @throws IllegalArgumentException if unable to create due to input arguments.
	 */
	public static Cidr cidr(InetAddress addr, int suffix) throws IllegalArgumentException {
		if (suffix < 0) {
			throw new IllegalArgumentException("Invalid mask length! [" + suffix + "]");
		}
		if (addr instanceof Inet4Address) {
			if (suffix > 32) {
				throw new IllegalArgumentException("Invalid mask length! [" + suffix + "]");
			}
		}
		if (suffix > 128) {  // IPv6
			throw new IllegalArgumentException("Invalid mask length! [" + suffix + "]");
		}
		return new Cidr(addr, suffix);
	}

	/**
	 *
	 * @param cidr in {@code <IP>/<netmask>} format.
	 * @return a new instance representing arguments.
	 * @throws IllegalArgumentException if unable to create due to input arguments.
	 */
	public static Cidr cidr(String cidr) throws IllegalArgumentException {
		final int p = cidr.indexOf("/");
		if (p < 0) {
			throw new IllegalArgumentException("Invalid CIDR notation! [" + cidr + "]");
		}
		try {
			final InetAddress addr = InetAddress.getByName(cidr.substring(0, p));
			return cidr(addr, Integer.valueOf(cidr.substring(p + 1)));
		} catch (UnknownHostException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private static InetAddress toIPv4Address(BigInteger addr) {
		final int b = addr.intValue();
		try {
			byte[] a = new byte[4];
			a[0] = (byte) (b >> 24 & 0xFF);
			a[1] = (byte) (b >> 16 & 0xFF);
			a[2] = (byte) (b >> 8 & 0xFF);
			a[3] = (byte) (b & 0xFF);
			return InetAddress.getByAddress(a);
		} catch (UnknownHostException e) {
			throw new IllegalStateException(e);
		}
	}

	private static InetAddress toIPv6Address(BigInteger addr) {
		final byte[] b = addr.toByteArray();
		if (b.length > 16 && !(b.length == 17 && b[0] == 0)) {
			throw new IllegalStateException("Invalid IPv6 address!");
		}
		try {
			if (b.length == 16) {
				return InetAddress.getByAddress(b);
			}
			final byte[] a = new byte[16];
			if (b.length == 17) {
				System.arraycopy(b, 1, a, 0, 16);
			} else {
				int p = 16 - b.length;
				System.arraycopy(b, 0, a, p, b.length);
			}
			return InetAddress.getByAddress(a);
		} catch (UnknownHostException e) {
			throw new IllegalStateException(e);
		}
	}

	protected static byte[] toIPv4Bytes(Inet6Address addr) {
		final byte[] bytes = addr.getAddress();
		for (int i = 0; i < 9; i ++) {
			if (bytes[i] != 0) {
				throw new IllegalArgumentException("Unable to convert to IPv4! [" + addr + "]");
			}
		}
		if (bytes[10] != 0 && bytes[10] != 0xFF || bytes[11] != 0 && bytes[11] != 0xFF) {
			throw new IllegalArgumentException("Unable to convert to IPv4! [" + addr + "]");
		}
		return Arrays.copyOfRange(bytes, 0, 4);
	}

	private static byte[] toIPv6Bytes(Inet4Address addr) {
		final byte[] bytes = new byte[16];
		System.arraycopy(addr.getAddress(), 0, bytes, 12, 4);
		return bytes;
	}
}
