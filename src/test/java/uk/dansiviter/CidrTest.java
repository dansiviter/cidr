/*
 * Copyright 2020 Daniel Siviter
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.dansiviter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;

import org.junit.jupiter.api.Test;

/**
 * Unit test for {@link Cidr}.
 *
 * @author Daniel Siviter
 * @since v1.0 [16 Feb 2020]
 */
public class CidrTest {
	@Test
	public void ipv4() throws UnknownHostException {
		Cidr cidr = Cidr.cidr("192.168.100.14/24");
		assertEquals(InetAddress.getByName("192.168.100.0"), cidr.start());
		assertEquals(InetAddress.getByName("192.168.100.255"), cidr.end());
		assertEquals(InetAddress.getByName("255.255.255.0"), cidr.netmask());
		assertEquals(24, cidr.suffix());
		assertEquals(BigInteger.valueOf(256), cidr.length());
		assertFalse(cidr.contains(InetAddress.getByName("192.168.99.14")));
		assertTrue(cidr.contains(InetAddress.getByName("192.168.100.14")));
		assertFalse(cidr.contains(InetAddress.getByName("192.168.101.14")));

		// comparision
		assertEquals(1, cidr.compareTo(Cidr.cidr("192.168.100.14/25")));
		assertEquals(1, cidr.compareTo(Cidr.cidr("192.168.101.14/24")));
		assertEquals(0, cidr.compareTo(Cidr.cidr("192.168.100.14/24")));
		assertEquals(-1, cidr.compareTo(Cidr.cidr("192.168.100.14/23")));
		assertEquals(-1, cidr.compareTo(Cidr.cidr("192.168.99.14/24")));

		assertEquals(1, cidr.compareTo(Cidr.cidr("0:0:0:0:3::/96")));

		// equality
		assertTrue(cidr.equals(Cidr.cidr("192.168.100.14/24")));
		assertFalse(cidr.equals(Cidr.cidr("192.168.100.14/25")));
		assertEquals(1415847897, cidr.hashCode());

		// test limits
		Cidr.cidr("0.0.0.0/0");
		Cidr.cidr("0.0.0.0/32");
		assertThrows(IllegalArgumentException.class, () -> Cidr.cidr("0.0.0.0/-1"));
		assertThrows(IllegalArgumentException.class, () -> Cidr.cidr("0.0.0.0/33"));
	}

	@Test
	public void ipv6() throws UnknownHostException {
		Cidr cidr = Cidr.cidr("2001:db8:0:0:3::/48");
		assertEquals(InetAddress.getByName("2001:db8:0:0:0:0:0:0"), cidr.start());
		assertEquals(InetAddress.getByName("2001:db8:0:ffff:ffff:ffff:ffff:ffff"), cidr.end());
		assertEquals(InetAddress.getByName("0:0:ff:0:0:0:0:0"), cidr.netmask());
		assertEquals(48, cidr.suffix());
		assertEquals(new BigInteger("1208925819614629174706176"), cidr.length());
		assertFalse(cidr.contains(InetAddress.getByName("2001:db7:0:0:0:0:0:2")));
		assertTrue(cidr.contains(InetAddress.getByName("2001:db8:0:0:0:0:0:2")));
		assertFalse(cidr.contains(InetAddress.getByName("2001:db9:0:0:0:0:0:2")));

		// comparision
		assertEquals(1, cidr.compareTo(Cidr.cidr("2001:db8:0:0:3::/49")));
		assertEquals(1, cidr.compareTo(Cidr.cidr("2001:db9:0:0:4::/48")));
		assertEquals(0, cidr.compareTo(Cidr.cidr("2001:db8:0:0:3:0:0:0/48")));
		assertEquals(-1, cidr.compareTo(Cidr.cidr("2001:db8:0:0:3::/47")));
		assertEquals(-1, cidr.compareTo(Cidr.cidr("2001:db7:0:0:2::/48")));

		assertEquals(-1, cidr.compareTo(Cidr.cidr("192.168.100.14/24")));

		// equality
		assertTrue(cidr.equals(Cidr.cidr("2001:db8:0:0:3::/48")));
		assertFalse(cidr.equals(Cidr.cidr("2001:db8:0:0:3::/49")));
		assertEquals(-120359511, cidr.hashCode());

		// test limits
		Cidr.cidr("::/0");
		Cidr.cidr("::/128");
		assertThrows(IllegalArgumentException.class, () -> Cidr.cidr("::/-1"));
		assertThrows(IllegalArgumentException.class, () -> Cidr.cidr("::/129"));
	}
}
